package com.alehandro.easycoretestproject.DI;

import com.alehandro.easycoretestproject.Presenters.ContactDetailsFragmentPresenter;
import com.alehandro.easycoretestproject.Presenters.ContactListFragmentPresenter;
import com.alehandro.easycoretestproject.Presenters.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alehandro on 28.01.2017.
 */
@Module
public class ViewModule {

    @Provides
    @Singleton
    MainActivityPresenter getMainActivityPresenter(){
        return new MainActivityPresenter();
    }

    @Provides
    @Singleton
    ContactDetailsFragmentPresenter getContactDetailsFragmentPresenter(){
        return new ContactDetailsFragmentPresenter();
    }

    @Provides
    @Singleton
    ContactListFragmentPresenter getContactListFragmentPresenter(){
        return new ContactListFragmentPresenter();
    }


}
