package com.alehandro.easycoretestproject.DI;

import com.alehandro.easycoretestproject.Adapters.ContactListAdapter;
import com.alehandro.easycoretestproject.UI.ContactDetailsFragment;
import com.alehandro.easycoretestproject.UI.ContactListFragment;
import com.alehandro.easycoretestproject.UI.ContactsListViewHolder;
import com.alehandro.easycoretestproject.UI.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 28.01.2017.
 */
@Singleton
@Component(modules = ViewModule.class)
public interface ViewComponent {
    void inject (MainActivity mainActivity);
    void inject (ContactListFragment contactListFragment);
    void inject (ContactDetailsFragment contactDetailsFragment);
    }
