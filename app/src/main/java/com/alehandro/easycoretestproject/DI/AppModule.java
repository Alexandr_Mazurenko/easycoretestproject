package com.alehandro.easycoretestproject.DI;

import android.app.Application;
import android.support.v7.widget.LinearLayoutManager;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.Interfaces.IRetrofitService;
import com.alehandro.easycoretestproject.Model.Model;
import com.alehandro.easycoretestproject.Presenters.ContactListAdapterPresenter;
import com.alehandro.easycoretestproject.Presenters.ContactListFragmentPresenter;
import com.alehandro.easycoretestproject.Presenters.MainActivityPresenter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alehandro on 28.01.2017.
 */
@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }


    Gson getGson(){
        return new GsonBuilder().setLenient().create();
    }

    OkHttpClient getOkHttpClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .retryOnConnectionFailure(true)
                .build();
    }


    Retrofit getRetrofit(){
        return   new Retrofit.Builder()
                .baseUrl(AppConstants.SERVER_URL)
                .client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    IRetrofitService getRetrofitService(){
        return getRetrofit().create(IRetrofitService.class);
    }

    @Provides
    @Singleton
    LinearLayoutManager getLinearLayoutManager(Application application){
        return new LinearLayoutManager(application);
    }

    @Provides
    @Singleton
    ContactListAdapterPresenter  getContactListAdapterPresenter(){
        return new ContactListAdapterPresenter();
    }

    @Provides
    @Singleton
    ContactListFragmentPresenter getContactListFragmentPresenter(){
        return new ContactListFragmentPresenter();
    }

    @Provides
    @Singleton
    MainActivityPresenter getMainActivityPresenter(){
        return new MainActivityPresenter();
    }

    @Provides
    @Singleton
    Model getModel (){
        return new Model();
    }

    }
