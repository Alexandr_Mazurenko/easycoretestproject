package com.alehandro.easycoretestproject.DI;

import com.alehandro.easycoretestproject.Adapters.ContactListAdapter;
import com.alehandro.easycoretestproject.Presenters.ContactDetailsFragmentPresenter;
import com.alehandro.easycoretestproject.Presenters.ContactListAdapterPresenter;
import com.alehandro.easycoretestproject.Presenters.ContactListFragmentPresenter;
import com.alehandro.easycoretestproject.Presenters.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 28.01.2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(ContactDetailsFragmentPresenter contactDetailsFragmentPresenter);
    void inject (ContactListFragmentPresenter contactListFragmentPresenter);
    void inject (ContactListAdapterPresenter contactListAdapterPresenter);
    void inject (MainActivityPresenter mainActivityPresenter);
    void inject (ContactListAdapter contactListAdapter);
        }
