package com.alehandro.easycoretestproject.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class Contact {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_phone")
    @Expose
    private String mobilePhone;
    @SerializedName("bussiness_phone")
    @Expose
    private String bussinessPhone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("about")
    @Expose
    private String about;

    public String getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public String getCompany() {
        return company;
    }

    public String getEmail() {
        return email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getBussinessPhone() {
        return bussinessPhone;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public String getAbout() {
        return about;
    }
}
