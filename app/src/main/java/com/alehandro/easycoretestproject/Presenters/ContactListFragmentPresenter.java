package com.alehandro.easycoretestproject.Presenters;

import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IRetrofitService;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Interfaces.IViewPresenter;
import com.alehandro.easycoretestproject.Model.Model;
import com.alehandro.easycoretestproject.POJO.Contact;

import org.reactivestreams.Subscription;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class ContactListFragmentPresenter implements IViewPresenter {
    private WeakReference<IView> mIViewWeakReference;
    @Inject
    Model model;
    @Inject
    IRetrofitService retrofitService;
    @Inject
    LinearLayoutManager linearLayoutManager;
    private Subscription mSubscription;
    private Observer<List<Contact>> mContactObserver;


    public ContactListFragmentPresenter() {
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
        }

    public void getContacts() {
        if (model.getContactsList().size() == 0) {
            getContactsFromWeb();
        } else {mIViewWeakReference.get().updateUI();}
    }

    void getContactsFromWeb() {
        mContactObserver = new Observer<List<Contact>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Contact> contacts) {
                model.setContactsList(contacts);
            }

            @Override
            public void onError(Throwable e) {
                Log.i(AppConstants.LOG_TAG, "Error = " + e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {
                mIViewWeakReference.get().updateUI();
                for (Contact contact : model.getContactsList()) {
                    Log.i(AppConstants.LOG_TAG, "Firstname:" + contact.getFirstname()
                            + "\n Surname:" + contact.getSurname()
                            + "\n Company:" + contact.getCompany()
                            + "\n Email:" + contact.getEmail()
                            + "\n MobilePhone:" + contact.getMobilePhone()
                            + "\n BusinessPhone:" + contact.getBussinessPhone()
                            + "\n Adress:" + contact.getAddress()
                            + "\n WebSite:" + contact.getWebsiteUrl()
                            + "\n About:" + contact.getAbout());
                }
            }
        };
        Observable<List<Contact>> getContacts = retrofitService.getContacts();
        getContacts.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mContactObserver);


    }
}
