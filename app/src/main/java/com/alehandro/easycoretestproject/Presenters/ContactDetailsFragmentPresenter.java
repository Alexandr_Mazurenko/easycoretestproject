package com.alehandro.easycoretestproject.Presenters;

import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Interfaces.IViewPresenter;
import com.alehandro.easycoretestproject.Interfaces.OnHolderItemClickListener;
import com.alehandro.easycoretestproject.Model.Model;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class ContactDetailsFragmentPresenter implements IViewPresenter {
    private WeakReference<IView> mIViewWeakReference;
    @Inject
    Model model;


    public ContactDetailsFragmentPresenter() {
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
    }

    public int getChosenPosition(){
        return model.getChosenPosition();
    }

    public String getContactName(int position){
        return model.getContactsList().get(position).getFirstname()+
                " "+model.getContactsList().get(position).getSurname();
    }

    public String getCompany (int position){
        return "at "+model.getContactsList().get(position).getCompany();
    }

    public String getEmail(int position){
        return model.getContactsList().get(position).getEmail();
    }

    public String getBusinessPhoneNumber(int position){
        return "business"+model.getContactsList().get(position).getBussinessPhone();
    }

    public String getMobilePhoneNumber(int position){
        return "home   "+model.getContactsList().get(position).getMobilePhone();
    }

    public String getHomeAdress(int position){
        return model.getContactsList().get(position).getAddress();
    }

    public String getWebSite(int position){
        return model.getContactsList().get(position).getWebsiteUrl();
    }
}
