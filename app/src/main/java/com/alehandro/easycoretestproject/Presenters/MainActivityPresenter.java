package com.alehandro.easycoretestproject.Presenters;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Interfaces.IViewPresenter;
import com.alehandro.easycoretestproject.Model.Model;
import com.alehandro.easycoretestproject.UI.ContactDetailsFragment;
import com.alehandro.easycoretestproject.UI.ContactListFragment;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class MainActivityPresenter implements IViewPresenter{
    private WeakReference<IView>  mIViewWeakReference;
    @Inject
    Model model;

    public MainActivityPresenter (){
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
        }

    public Fragment getCurrentFragment(){
        if(model.getCurrentFragment().equals(AppConstants.CONTACTS_LIST_FRAGMENT)){
            return new ContactListFragment();
        }else
            return  new ContactDetailsFragment();
    }

    public void setActivityContext(Context context){
        model.setActivityContext(context);
    }

    public void setChosenPosition (int position){
        model.setChosenPosition(position);
    }

    public  void setCurrentFragment(String currentFragment){
        model.setCurrentFragment(currentFragment);
    }

    public String getToolbarTitle(){
        return model.getToolBarTitle();
    }

    public void setToolbarTitle(String title){
        model.setToolBarTitle(title);
    }

    public void switchToDetail(int position){
        setChosenPosition(position);
        setCurrentFragment(AppConstants.DETAILS_FRAGMENT);
        setToolbarTitle(AppConstants.DETAIL_TOOLBAR_TITLE);
        mIViewWeakReference.get().updateUI();
    }

    public void switchToList(){
        setCurrentFragment(AppConstants.CONTACTS_LIST_FRAGMENT);
        setToolbarTitle(AppConstants.LIST_TOOLBAR_TITLE);
        mIViewWeakReference.get().updateUI();
    }

}
