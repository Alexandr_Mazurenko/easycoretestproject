package com.alehandro.easycoretestproject.Presenters;

import android.content.Context;

import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IListAdapter;
import com.alehandro.easycoretestproject.Model.Model;

import javax.inject.Inject;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class ContactListAdapterPresenter implements IListAdapter {
    @Inject
    Model model;

    public ContactListAdapterPresenter() {
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public int getItemCount() {
        return model.getContactsList().size();
    }

    public String getName(int position){
        return model.getContactsList().get(position).getFirstname()
                +" "+ model.getContactsList().get(position).getSurname();
    }

    public String getBusinessPhone(int position){
        return model.getContactsList().get(position).getBussinessPhone();
    }

    public String getAdress(int position){
        return model.getContactsList().get(position).getAddress();
    }

    public Context getActivityContext(){
        return model.getActivityContext();
    }
}
