package com.alehandro.easycoretestproject.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.OnHolderItemClickListener;
import com.alehandro.easycoretestproject.Presenters.ContactListAdapterPresenter;
import com.alehandro.easycoretestproject.Presenters.MainActivityPresenter;
import com.alehandro.easycoretestproject.R;
import com.alehandro.easycoretestproject.UI.ContactsListViewHolder;
import com.alehandro.easycoretestproject.UI.MainActivity;

import javax.inject.Inject;

/**
 * Created by Alehandro on 31.01.2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactsListViewHolder> {
    @Inject
    ContactListAdapterPresenter presenter;

    public ContactListAdapter(){
        MyApplication.getAppComponent().inject(this);
    }

    @Override
    public ContactsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return  new ContactsListViewHolder(LayoutInflater.from(presenter.getActivityContext())
                .inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ContactsListViewHolder holder, int position) {
        holder.contactName.setText(presenter.getName(position));
        holder.contactPhone.setText(presenter.getBusinessPhone(position));
        holder.contactAdress.setText(presenter.getAdress(position));


    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
