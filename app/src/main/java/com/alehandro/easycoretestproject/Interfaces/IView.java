package com.alehandro.easycoretestproject.Interfaces;

/**
 * Created by Alehandro on 28.01.2017.
 */

public interface IView {
    void initUI();
    void updateUI();
}
