package com.alehandro.easycoretestproject.Interfaces;

import com.alehandro.easycoretestproject.POJO.Contact;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by Alehandro on 28.01.2017.
 */

public interface IRetrofitService {
    @GET("api/contacts.json")
    Observable<List<Contact>> getContacts();
}
