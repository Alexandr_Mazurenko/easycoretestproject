package com.alehandro.easycoretestproject.Interfaces;

/**
 * Created by Alehandro on 31.01.2017.
 */
@FunctionalInterface
public interface OnHolderItemClickListener {
    void onHolderItemClicked(int position);
}
