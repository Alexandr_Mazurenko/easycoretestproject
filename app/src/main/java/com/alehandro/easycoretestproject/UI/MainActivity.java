package com.alehandro.easycoretestproject.UI;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Interfaces.OnHolderItemClickListener;
import com.alehandro.easycoretestproject.Presenters.MainActivityPresenter;
import com.alehandro.easycoretestproject.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IView, OnHolderItemClickListener{
    @Inject
    MainActivityPresenter presenter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getViewComponent().inject(this);
        presenter.setView(this);
        presenter.setActivityContext(this);
        setContentView(R.layout.activity_main);
        initUI();
        updateUI();
    }

    @Override
    public void initUI() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

    @Override
    public void updateUI() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,presenter.getCurrentFragment())
                .commit();
        getSupportActionBar().setTitle(presenter.getToolbarTitle());
    }

    @Override
    public void onHolderItemClicked(int position) {
        presenter.switchToDetail(position);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

   }

    @Override
    public void onBackPressed() {
        if(presenter.getToolbarTitle().equals(AppConstants.DETAIL_TOOLBAR_TITLE)){
            presenter.switchToList();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }else {
            super.onBackPressed();
        }
    }
}
