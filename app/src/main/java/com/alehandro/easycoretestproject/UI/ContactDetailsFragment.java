package com.alehandro.easycoretestproject.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Presenters.ContactDetailsFragmentPresenter;
import com.alehandro.easycoretestproject.R;

import org.w3c.dom.Text;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class ContactDetailsFragment extends Fragment implements IView {
    @Inject
    ContactDetailsFragmentPresenter presenter;
    @BindView(R.id.detailContactName)
    TextView contactName;
    @BindView(R.id.detailContactCompany)
    TextView contactCompany;
    @BindView(R.id.detailContactEmail)
    TextView contactEmail;
    @BindView(R.id.detailContactBusinessPhoneNumber)
    TextView contactBusinessPhoneNumber;
    @BindView(R.id.detailContactMobilePhoneNumber)
    TextView contactMobilePhoneNumber;
    @BindView(R.id.detailContactHomeAdress)
    TextView contactHomeAdress;
    @BindView(R.id.detailContactWebSite)
    TextView contactWebSite;
    private View mView;
    private int chosenPosition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MyApplication.getViewComponent().inject(this);
        presenter.setView(this);
        mView = inflater.inflate(R.layout.detail_fragment, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this,view);
        updateUI();
    }

    @Override
    public void initUI() {}

    @Override
    public void updateUI() {
        chosenPosition = presenter.getChosenPosition();
        contactName.setText(presenter.getContactName(chosenPosition));
        contactCompany.setText(presenter.getCompany(chosenPosition));
        contactEmail.setText(presenter.getEmail(chosenPosition));
        contactBusinessPhoneNumber.setText(presenter.getBusinessPhoneNumber(chosenPosition));
        contactMobilePhoneNumber.setText(presenter.getMobilePhoneNumber(chosenPosition));
        contactHomeAdress.setText(presenter.getHomeAdress(chosenPosition));
        contactWebSite.setText(presenter.getWebSite(chosenPosition));
    }
}
