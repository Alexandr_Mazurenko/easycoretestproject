package com.alehandro.easycoretestproject.UI;

import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alehandro.easycoretestproject.Adapters.ContactListAdapter;
import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.DI.MyApplication;
import com.alehandro.easycoretestproject.Helper.RecyclerItemClickListener;
import com.alehandro.easycoretestproject.Interfaces.IView;
import com.alehandro.easycoretestproject.Interfaces.OnHolderItemClickListener;
import com.alehandro.easycoretestproject.Presenters.ContactListFragmentPresenter;
import com.alehandro.easycoretestproject.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class ContactListFragment extends Fragment implements IView {
    @Inject
    ContactListFragmentPresenter presenter;
    private View mView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ContactListAdapter adapter;
    private LinearLayoutManager layoutManager;
    OnHolderItemClickListener clickListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MyApplication.getViewComponent().inject(this);
        presenter.setView(this);
        mView = inflater.inflate(R.layout.list_fragment,container,false);
        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        clickListener = (MainActivity)activity;
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initUI();
        presenter.getContacts();
    }

    @Override
    public void initUI() {
        recyclerView.setHasFixedSize(false);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation()));
        adapter = new ContactListAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), (view, position) -> {
                   Log.i(AppConstants.LOG_TAG,"position:"+position);
                    clickListener.onHolderItemClicked(position);
                })
        );

    }

    @Override
    public void updateUI() {
        adapter.notifyDataSetChanged();
    }
}
