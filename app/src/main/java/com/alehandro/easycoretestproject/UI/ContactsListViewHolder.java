package com.alehandro.easycoretestproject.UI;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alehandro.easycoretestproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class ContactsListViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.photoHolder)
    public ImageView contactPhoto;
    @BindView(R.id.userName)
    public TextView contactName;
    @BindView(R.id.userPhone)
    public TextView contactPhone;
    @BindView(R.id.userAdress)
    public TextView contactAdress;
    @BindView(R.id.infoIcon)
    public ImageView iconInfo;
    public ContactsListViewHolder(View itemView) {
           super(itemView);

        ButterKnife.bind(this, itemView);
    }
}
