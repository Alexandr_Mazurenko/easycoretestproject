package com.alehandro.easycoretestproject.Model;

import android.content.Context;

import com.alehandro.easycoretestproject.Constants.AppConstants;
import com.alehandro.easycoretestproject.POJO.Contact;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;

/**
 * Created by Alehandro on 29.01.2017.
 */

public class Model {
    private List<Contact> contactsList;
    private String currentFragment;
    private int chosenPosition;
    private String toolBarTitle;

    public Context getActivityContext() {
        return activityContext;
    }

    public void setActivityContext(Context activityContext) {
        this.activityContext = activityContext;
    }

    private Context activityContext;

    public Model (){
        contactsList = new ArrayList<>();
        currentFragment = AppConstants.CONTACTS_LIST_FRAGMENT;
        toolBarTitle = AppConstants.LIST_TOOLBAR_TITLE;
    }

    public List<Contact> getContactsList() {
        return contactsList;
    }

    public void setContactsList(List<Contact> contactsList) {
        this.contactsList = contactsList;
    }

    public String getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }

    public int getChosenPosition() {
        return chosenPosition;
    }

    public void setChosenPosition(int chosenPosition) {
        this.chosenPosition = chosenPosition;
    }

    public String getToolBarTitle() {
        return toolBarTitle;
    }

    public void setToolBarTitle(String toolBarTitle) {
        this.toolBarTitle = toolBarTitle;
    }
}
