package com.alehandro.easycoretestproject.Constants;

/**
 * Created by Alehandro on 28.01.2017.
 */

public class AppConstants {
    public final static String SERVER_URL = "https://easycore-ios-example.firebaseapp.com";
    public final static String CONTACTS_LIST_FRAGMENT = "contactsList";
    public final static String DETAILS_FRAGMENT = "detailsList";
    public final static String LOG_TAG = "EasyCoreTestProject";
    public final static String LIST_TOOLBAR_TITLE = "Contacts";
    public final static String DETAIL_TOOLBAR_TITLE = "Detail";
}
